package idontknow;

import javax.jws.WebService;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * The concrete implementation of the `IDontKnow` service
 * @author Elias Frantar
 * @version 2015-04-15
 */
@WebService(endpointInterface = "idontknow.IDontKnow")
public class IDontKnowImpl implements IDontKnow {

    @Override
    public String query(String tag) {
        System.out.println("Called `query()` with tag: \"" + tag + "\"");

        try {
            URL url = new URL("http://localhost:5000/q?tag=" + tag);
            URLConnection conn = url.openConnection();
            InputStream is = conn.getInputStream();

            return convertStreamToString(is);
        } catch(Exception e) {
            e.printStackTrace();
            return "Error: " + e.getMessage();
        }
    }

    /* http://stackoverflow.com/questions/309424/read-convert-an-inputstream-to-a-string */
    private static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

}
