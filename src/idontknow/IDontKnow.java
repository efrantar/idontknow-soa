package idontknow;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

/**
 * The interface of the `IDontKnow` service
 * @author Elias Frantar
 * @version 2015-04-15
 */
@WebService
@SOAPBinding(style = Style.RPC)
public interface IDontKnow {

    /**
     * Queries all entries with the given
     * @param tag the tag to search
     * @return all found entries as a JSON-String
     */
    @WebMethod String query(String tag);

}
