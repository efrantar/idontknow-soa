package idontknow;

import javax.xml.ws.Endpoint;

/**
 * Publishes the `IDontKnow` service
 * @author Elias Frantar
 * @version 2015-04-15
 */
public class IDontKnowPublisher {
    public static void main(String[] args) {
        try {
            String url = "http://localhost:9999/ws/idontknow";
            Endpoint.publish(url, new IDontKnowImpl());
            System.out.println("Published `IDontKnowImpl` at \"" + url + "\"");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
