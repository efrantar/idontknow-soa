package idontknow;

import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

/**
 * Uses the `IDontKnow` service
 * @author Elias Frantar
 * @version 2015-04-15
 */
public class IDontKnowClient {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            System.out.println("usage: IDontKnowClient <tag>");
            return;
        }

        try {
            URL url = new URL("http://localhost:9999/ws/idontknow?wsdl");
            QName qname = new QName("http://idontknow/", "IDontKnowImplService");

            Service service = Service.create(url, qname);
            IDontKnow idontknow = service.getPort(IDontKnow.class);

            System.out.println(idontknow.query(args[0]));
        } catch (Exception e) {
            System.out.println("Something went wrong: " + e.getMessage());
        }
    }

}
